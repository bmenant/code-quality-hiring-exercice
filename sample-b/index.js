import { createClient } from 'redis';
import moment from 'moment';
import fetcher from './fetcher';

const redis = createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const pubsubChan = process.env.REDIS_PUBSUB_CHANNEL;
const sub = process.env.DEBUG && redis.duplicate();

if (process.env.DEBUG) {
  sub.on('message', (channel, message) => {
    // console.log(`Msg received on "${channel}": ${message}`);
    console.log(`Msg received on "${channel}"`);
  });
  sub.subscribe(pubsubChan);
}

const formId = process.env.TYPEFORM_FORM_ID;
const token = process.env.TYPEFORM_TOKEN;
const datetimeToBeginFetching =
  moment(process.env.DATETIME_BEGIN_FETCHING).format('Y-MM-DDTHH:mm:ss');

// Setup Fetcher
const fetchResponses = fetcher({formId, token});

function handleResponses({ items }, firstItem) {
  if (items.length === 0) return Promise.resolve(firstItem);

  const lastItem = items[items.length-1];

  items
    .map(item => JSON.stringify(item))
    .forEach(item => redis.publish(pubsubChan, item));

  return fetchResponses({ since: datetimeToBeginFetching, before: lastItem.token })
    .then(response => handleResponses(response, firstItem || items[0]));
}

redis.get('newest_fetched_item', (err, newestItem) => {
  fetchResponses({ since: datetimeToBeginFetching, after: newestItem })
    .then(handleResponses)
    .then(nextNewestItem => new Promise((resolve, reject) => {
      if (!nextNewestItem) return resolve();
      return redis.set('newest_fetched_item', nextNewestItem.token, err => err ? reject(err) : resolve())
    }))
    .then(() => {
      if (process.env.DEBUG) sub.unsubscribe() && sub.quit();
      redis.quit()
    })
    .catch(err => {
      console.error(err);
      if (process.env.DEBUG) sub.unsubscribe() && sub.quit();
      redis.quit()
    });
});
