export default class ApiConsumer {

  constructor({ fetchResponses, formatResponse, newestItemStore, publish }) {
    this.fetchResponses = fetchResponses;
    this.formatResponse = formatResponse;
    this.newestItemStore = newestItemStore;
    this.publish = publish;
  }

  handleResponses({ items }, firstItem, fetchBefore) {
    if (items.length === 0) return Promise.resolve(firstItem);

    this.publish(
      items.map(this.formatResponse));

    const lastItem = items[items.length-1];
    return fetchBefore(lastItem.token)
      .then(response =>
        this.handleResponses(response, firstItem || items[0], fetchBefore));
  }

  pull(since) {
    return this.newestItemStore.get()
      .then(newestItem =>
        this.fetchResponses({since, after: newestItem}))

      .then(responses =>
        this.handleResponses(responses, null,
            before => this.fetchResponses({since, before})))

      .then(nextNewestItem =>
        new Promise((resolve, reject) =>
          !nextNewestItem ?
            reject('Already up to date!') :
            resolve(this.newestItemStore.set(nextNewestItem.token))));
  }
}
