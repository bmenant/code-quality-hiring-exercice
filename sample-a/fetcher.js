import fetch, { Headers } from 'node-fetch';
import moment from 'moment';

function responseToJson(response) {
  if (!response.ok) throw new Error('Oops, network response wasn’t ok!');

  const contentType = response.headers.get('content-type');
  if (contentType && contentType.includes('application/json')) {
    return response.json();
  }

  throw new TypeError('Oops, we haven’t got JSON!');
}

function paramsToQuerystring(paramsMap) {
  return Array.from(paramsMap.entries())
    .reduce((acc, [key, value]) => {
      const param = `${key}=${encodeURIComponent(value)}`;
      return acc ? `${acc}&${param}` : param;
    }, '');
}

export default function fetchTypeformResponses({formId, token}) {
  const apiUrl = `https://api.typeform.com/forms/${formId}/responses`;
  const headers = new Headers({ authorization: `bearer ${token}` });

  const paramsMap = new Map()
    .set('completed', true)
    .set('page_size', 100);

  return ({ since, before, after }) => {
    if (since) paramsMap.set('since', moment(since).format('Y-MM-DDTHH:mm:ss'));
    if (before) paramsMap.set('before', before);
    if (after) paramsMap.set('after', after);

    return fetch(`${apiUrl}?${paramsToQuerystring(paramsMap)}`, { headers })
      .then(responseToJson)
  }
}
