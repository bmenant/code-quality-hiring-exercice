import redis from 'redis';

export default class Publisher {

  constructor({ port, host, channel }) {
    this.publisher = redis.createClient(port, host);
    this.channel = channel;

    if (process.env.DEBUG) this.enableLogger();

    this.publishStr = this.publishStr.bind(this);
    this.publishObj = this.publishObj.bind(this);
    this.publishObjArr = this.publishObjArr.bind(this);
  }

  enableLogger() {
    this.subscriber = this.publisher.duplicate();

    this.subscriber.on('message', (ch, msg) => {
      console.log(`Msg received: ${msg}`);
      // console.log(`Msg received!`);
    });

    this.subscriber.subscribe(this.channel);
  }

  publishStr(str) {
    this.publisher.publish(this.channel, str);
  }

  publishObj(obj) {
    this.publishStr(JSON.stringify(obj));
  }

  publishObjArr(objArr) {
    objArr.forEach(this.publishObj);
  }

  quit() {
    const { publisher, subscriber } = this;

    publisher.quit();
    if (subscriber) {
      subscriber.unsubscribe();
      subscriber.quit();
    }
  }
}
