import redis from 'redis';

export default class RedisClientGetSet {

  constructor({ port, host, keyName}) {
    this.keyName = keyName;
    this.client = redis.createClient(port, host);
  }

  quit() {
    this.client.quit();
  }

  get() {
    return new Promise((resolve, reject) => {
      this.client.get(this.keyName, (err, item) => {
        if (err) return reject(err);
        resolve(item);
      });
    })
  }

  set(value) {
    return new Promise((resolve, reject) => {
      this.client.set(this.keyName, value, err => {
        if (err) return reject(err);
        resolve();
      });
    });
  }
}
