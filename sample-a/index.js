import fetcher from './fetcher';
import StoreGetSet from './Store';
import Publisher from './Publisher';
import ApiConsumer from './Consumer';
import formatter from './formatter'; // purposely not included within this exercice

const port = process.env.REDIS_PORT;
const host = process.env.REDIS_HOST;
const channel = process.env.REDIS_PUBSUB_CHANNEL;
const keyName = process.env.REDIS_KEY_NAME;

const storeGetSet = new StoreGetSet({ port, host, keyName });
const publisher = new Publisher({ port, host, channel });

const sinceLimitDatetime = new Date(process.env.DATETIME_BEGIN_FETCHING);

const formId = process.env.TYPEFORM_FORM_ID;
const token = process.env.TYPEFORM_TOKEN;
const fetchResponses = fetcher({ formId, token });

const consumer = new ApiConsumer({
  newestItemStore: storeGetSet,
  publish: publisher.publishObjArr,
  formatResponse: formatter,
  fetchResponses,
});

consumer.pull(sinceLimitDatetime)
  .then(() => {
    publisher.quit();
    storeGetSet.quit();
  })
  .catch(err => {
    console.error(err);

    publisher.quit();
    storeGetSet.quit();
  });
